<?php

	//Include de todas las dependencias
	include "backend/requires.php";

	// Abro conexión a BBDD
	$objetoConexion = new DBConn();

	// Ejecutamos una query de recogida de productos
	$objetoProducto = new Product();

	$listaSeries = $objetoProducto->getAllSeries($objetoConexion);

	// Para cada serie, generamos un enlace de borrado
	foreach($listaSeries as $producto){
		echo $producto["nombre"] . ".... <a href='backend/controllers/Producto/DeleteProduct.php?id=".$producto["id"]."'>Borrar</a>";
		echo "<br />";
	}

	// Cierro conexión a BBDD
	$objetoConexion->close();

?>