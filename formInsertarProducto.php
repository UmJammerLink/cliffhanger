<!--Formulario para introducir un producto-->
<form action="backend/controllers/producto/insertProduct.php" method="POST" id="form">
	<fieldset class="scheduler-border">
		<legend>Inserción de Producto</legend>
		<div>
			<label for="id_nombre"><strong><em>Nombre:</em></strong></label>
			<input type="text" class="form-control" name="nombre" id="id_nombre">
		</div>
		<div>
			<label for="id_imagen"><strong><em>Imagen:</em></strong></label>
			<input type="text" class="form-control" name="imagen" id="id_imagen">
		</div>
		<div>
			<label for="id_descripcion"><strong><em>Descripción:</em></strong></label>
			<input type="text" class="form-control" name="descripcion" id="id_descripcion">
		</div>
		<div>
			<label for="id_tipo"><strong><em>Tipo:</em></strong></label>
			<select name="tipo" id="id_tipo">
				<option value="pelicula">Película</option>
				<option value="serie">Serie</option>
			</select>
		</div>
		<div>
			<label for="id_precio"><strong><em>Precio:</em></strong></label>
			<select name="precio" id="id_precio">
				<option value="2.99">2.99€</option>
				<option value="1.99">1.99€</option>
			</select>
		</div>
		<div>
			<input type="submit" value="Enviar formulario"/></div>
		</div>
	</fieldset>
</form>