<?php

	// Fichero properties
	require_once "properties.php";
	
	// Clase de conexión a BBDD
	require_once "models/DBConn/DBConn.class.php";

	// Clases del modelo
	require_once "models/Product/Product.class.php";	
	require_once "models/User/User.class.php";

?>