<?php

    // Include de todas las dependencias
	include "../../requires.php";

    // Se recogen los datos del formulario    
    $id = $_GET["id"];

	// Se abre conexión a BBDD
	$connectionObject = new DBConn();

	// Se ejecuta una query de borrado de usuario
    $userObject = new User();
    
    $insertUser = $userObject->deleteUser($connectionObject, $id);

    // Se cierra conexión a BBDD
	$connectionObject->close();

?>