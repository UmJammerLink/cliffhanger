<?php

    // Include de todas las dependencias
	include "../../requires.php";

    // Se recogen los datos del formulario    
    $email = $_POST["email"];
    $password = $_POST["password"];

	// Se abre conexión a BBDD
	$connectionObject = new DBConn();

	// Se ejecuta una query de inserción de usuario
    $userObject = new User();
    
    $insertUser = $userObject->insertUser($connectionObject, $email, $password);

    // Se cierra conexión a BBDD
	$connectionObject->close();

?>