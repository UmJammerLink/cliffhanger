<?php

    // Include de todas las dependencias
	include "../../requires.php";

    // Se recogen los datos del formulario
    $id          = $_POST["id"];
    $nombre      = $_POST["nombre"];
    $imagen      = $_POST["imagen"];
    $descripcion = $_POST["descripcion"];
    $tipo        = $_POST["tipo"];
    $precio      = $_POST["precio"];

	// Se abre conexión a BBDD
	$connectionObject = new DBConn();

	// Se ejecuta una query de actualización de producto
    $productObject = new Producto();
    
    $insertMovie = $productObject->updateProduct($connectionObject, $id, $nombre, $imagen, $descripcion, $tipo, $precio);

    // Se cierra conexión a BBDD
	$connectionObject->close();    

?>