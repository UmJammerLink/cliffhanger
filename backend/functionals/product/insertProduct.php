<?php

    // Include de todas las dependencias
	include "../../requires.php";

    // Se recogen los datos del formulario    
    $nombre = $_POST["nombre"];
    $imagen = $_POST["imagen"];
    $descripcion = $_POST["descripcion"];
    $tipo = $_POST["tipo"];
    $precio = $_POST["precio"];

	// Se abre conexión a BBDD
	$connectionObject = new DBConn();

	// Se ejecuta una query de inserción de producto
    $productObject = new Producto();
    
    $insertMovie = $productObject->insertProduct($connectionObject, $nombre, $imagen, $descripcion, $tipo, $precio);

    // Se cierra conexión a BBDD
	$connectionObject->close();    

?>