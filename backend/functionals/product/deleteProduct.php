<?php

    // Include de todas las dependencias
	include "../../requires.php";

    // Se recogen los datos del formulario    
    $id = $_GET["id"];

    // Se abre conexión a BBDD
	$connectionObject = new DBConn();

	// Se ejecuta una query de borrado de producto
    $productObject = new Producto();
    
    $deleteMovie = $productObject->deleteProduct($connectionObject, $id);

    // CierrSe cierra conexión a BBDD
	$connectionObject->close();

?>