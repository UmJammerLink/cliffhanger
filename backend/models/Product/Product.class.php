<?php

	class Product{

		// Método que recoge todos los productos
		public function getAllProducts($connection){
			return $connection->select("SELECT * FROM PRODUCTOS");
		}

		// Método que recoge todas las películas		
		public function getAllMovies($connection){
			return $connection->select("SELECT * FROM PRODUCTOS WHERE tipo = 'pelicula'");
		}

		// Método que recoge todas las series
		public function getAllSeries($connection){
			return $connection->select("SELECT * FROM PRODUCTOS WHERE tipo = 'serie'");
		}

		// Método que inserta un producto
		public function insertProduct($connection, $name, $image, $description, $type, $prize){
			return $connection->operation("INSERT INTO PRODUCTOS('nombre', 'imagen', 'descripcion', 'tipo', 'precio') VALUES('".$name."', '".$image."', '".$description."', '".$type."', ".$prize.")");
		}

		// Método que borra un producto
		public function deleteProduct($connection, $id){
			return $connection->operation("DELETE FROM PRODUCTOS WHERE ID = ".$id);
		}

		// Método que actualiza un producto
		public function updateProduct($connection, $id, $name, $image, $description, $type, $prize){
			return $connection->operation("UPDATE PRODUCTOS SET nombre = '".$name."', imagen = '".$image."', descripcion = '".$description."', tipo = '".$type."', precio = ".$prize." WHERE id = ".$id);
		}

	}

?>