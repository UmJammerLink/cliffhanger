<?php

	class DBConn {

		private $host;
		private $database;
		private $user;
		private $password;

		public $connection;

		// Método constructor de la clase. Se ejecuta cuando se crea el objeto
		function __construct(){

			//Inicializamos las propiedades del objeto
			$this->host     = $GLOBALS["host"];
			$this->database = $GLOBALS["database"];
			$this->user     = $GLOBALS["user"];
			$this->password = $GLOBALS["password"];

			// Abrimos conexión a BBDD
			$this->open();

		}

		// Método que abre la conexión a BBDD. Es privado porque se llama sólo al crear el objeto.
		private function open(){

			try{
				$this->connection = new mysqli($this->host, $this->user, $this->password, $this->database);
			}catch(Exception $error){
				print $error->getMessage() . "\n";
			}

		}

		// Método que permite el uso de consultas SELECT. Devuelve el array respuesta que podrá estar vacío si no hay filas, o con los datos correspondientes
		public function select($query){
			try{

				$methodResult = array();
				$queryResult = $this->connection->query($query);
				echo $queryResult;

				// Devuelve un array asociativo, del que sólo nos interesan los datos que corresponden a los alias que nos devuelve la consulta
				while($row = $queryResult->fetch_assoc()){
					array_push($methodResult, $row);
				}

				return $methodResult;

			}catch(Exception $error){
				print $error->getMessage() . "\n";
				return 0;
			}
		}

		// Método que permite el uso de las operaciones INSERT, UPDATE y DELETE. Devuelve 1 o 0 según si se ha podido realizar la operación o no
		public function operation($query){

			try{

				$methodResult = $this->connection->query($query);
				return $methodResult;

			}catch(Exception $error){
				print $error->getMessage() . "\n";
				return 0;
			}

		}

		// Método que cierra la conexión a BBDD
		public function close(){

			try{
				mysqli_close($this->connection);
			}catch(Exception $error){
				print $error->getMessage() . "\n";
			}

		}
	}

?>